import java.util.Scanner;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;

public class AES {

	public static void main(String args[]){
		if (args[0].equals("e"))
			encrypt(args[1], args[2]);
		else if (args[0].equals("d"))
			decrypt(args[1], args[2]);
		else
			System.out.println("Usage: [e|d] key inputFile");
	}
	
	public static void encrypt(String key, String inputFile){
		int[][] keySet = new int[60][4];
		
		Scanner keyScan = null;
		try{
			keyScan = new Scanner(new File(key));
		}catch (FileNotFoundException exFile) {
			System.out.println("key file not found!!!");
		}
		//////KEY GENERATE		
		String keyLine = "";
		if(keyScan.hasNextLine())
			keyLine = keyScan.nextLine();
		else
			System.out.println("key file empty");
		//Check for length = 64 (Hex chars) && Fill in the first 2 rounds keys
		if(keyLine.length()==64){
			for (int i=0; i<=31; i++){ //fill in the first 2 block of key
				keySet[i/4][i%4] = Integer.parseInt(keyLine.substring(i*2, i*2+2),16);
			}			
		}else{
			System.out.println("key length is : " + keyLine.length()+" hex chars");
		}

		KeyGen256 keyGen256 = new KeyGen256();
		keySet = keyGen256.generate(keySet);

		//////
		FileInputStream inputStream = null;
		try{
			inputStream = new FileInputStream(inputFile);
		}catch (FileNotFoundException exFileInput) {
			System.out.println("input file not found!!!");
			System.exit(1);
		}
		
		FileOutputStream outputStream = null;
		try{
			outputStream = new FileOutputStream(inputFile+".enc");
		}catch (FileNotFoundException exFileOutput) {
			System.out.println("enc output file tanked for some reason");
			System.exit(2);
		}
		
		byte[] byteblock = new byte[16];
		int[] intblock = new int[16];
		int[][] state = new int[4][4];
		SubByte subByte = new SubByte();
		MixColumn mixColumn = new MixColumn();
		
		long avai = 0;
		try{
			avai = inputStream.available();
		}catch (IOException inputstreamEX){
			System.out.println("inputStream not available ?");
			System.exit(3);
		}
		
		for (long l=0; l<avai/16; l++) {
			zero(byteblock);
			int read = 0;
			try{
				read = inputStream.read(byteblock);
			} catch (IOException ioreadinputEX) {
				System.out.println("error read inputStream byteblock");
			}
			toIntArray(byteblock, intblock);
			
			for (int i=0; i<=15; i++){
				state[i/4][i%4] = intblock[i];
			}
//			printstate(state);
			//round 0
			for (int i=0; i<=15; i++) {
				state[i/4][i%4] = state[i/4][i%4] ^ keySet[i/4][i%4]; 
			}
//			printstate(state);

			for(int r=1; r<=13 ; r++) { //round 1-13
				state = subByte.forward(state);
//				System.out.println("round "+ r +" after SubByte");
//				printstate(state);
				state = shiftRowsEnc(state);
//				System.out.println("round "+ r +" after shiftRows");
//				printstate(state);
				state = mixColumn.mixEnc(state);
//				System.out.println("round "+ r +" after mixColumns");
//				printstate(state);
				state = addRoundKey(state, keySet, r);
//				System.out.println("round "+ r +" after addRoundKey");
//				printstate(state);
			}
			//round 14
			state = subByte.forward(state);
//			System.out.println("round 14  after SubByte");
//			printstate(state);
			state = shiftRowsEnc(state);
//			System.out.println("round 14 after shiftRows");
//			printstate(state);
			state = addRoundKey(state, keySet, 14);
//			System.out.println("round 14 after addRoundKey");
//			printstate(state);
			
			for (int i=0; i<=15; i++) {
				byteblock[i] = (byte) (state[i/4][i%4] & 0xFF); 
			}
			try{
				outputStream.write(byteblock,0,read);
			} catch (IOException outputStreamEX) {
				System.out.println("Enc Output Stream error");
			}
		}
//		zero(byteblock);
//		int read =0;
//		try{
//				read = inputStream.read(byteblock);
//			} catch (IOException ioreadinputEX) {
//				System.out.println("error read inputStream byteblock");
//			}
//			toIntArray(byteblock, intblock);
//			
//			for (int i=0; i<=15; i++){
//				state[i/4][i%4] = intblock[i];
//			}
////			printstate(state);
//			//round 0
//			for (int i=0; i<=15; i++) {
//				state[i/4][i%4] = state[i/4][i%4] ^ keySet[i/4][i%4]; 
//			}
////			printstate(state);
//
//			for(int r=1; r<=13 ; r++) { //round 1-13
//				state = subByte.forward(state);
////				System.out.println("round "+ r +" after SubByte");
////				printstate(state);
//				state = shiftRowsEnc(state);
////				System.out.println("round "+ r +" after shiftRows");
////				printstate(state);
//				state = mixColumn.mixEnc(state);
////				System.out.println("round "+ r +" after mixColumns");
////				printstate(state);
//				state = addRoundKey(state, keySet, r);
////				System.out.println("round "+ r +" after addRoundKey");
////				printstate(state);
//			}
//			//round 14
//			state = subByte.forward(state);
////			System.out.println("round 14  after SubByte");
////			printstate(state);
//			state = shiftRowsEnc(state);
////			System.out.println("round 14 after shiftRows");
////			printstate(state);
//			state = addRoundKey(state, keySet, 14);
////			System.out.println("round 14 after addRoundKey");
////			printstate(state);
//			
//			for (int i=0; i<=15; i++) {
//				byteblock[i] = (byte) (state[i/4][i%4] & 0xFF); 
//			}
//			try{
//				outputStream.write(byteblock,0,read);
//			} catch (IOException outputStreamEX) {
//				System.out.println("Enc Output Stream error");
//			}

	}
	
	public static void decrypt(String key, String inputFile){
		int[][] keySet = new int[60][4];
		
		Scanner keyScan = null;
		try{
			keyScan = new Scanner(new File(key));
		}catch (FileNotFoundException kexFile) {
			System.out.println("key file not found!!!");
		}
		//////KEY GENERATE		
		String keyLine = "";
		if(keyScan.hasNextLine()) {
			keyLine = keyScan.nextLine();
		} else {
			System.out.println("key file empty");
		}		//Check for length = 64 (Hex chars) && Fill in the first 2 rounds keys
		if(keyLine.length()==64){
			for (int i=0; i<=31; i++){ //fill in the first 2 block of key
				keySet[i/4][i%4] = Integer.parseInt(keyLine.substring(i*2, i*2+2),16);
			}			
		} else {
			System.out.println("key length is : " + keyLine.length()+" hex chars");
		}

		KeyGen256 keyGen256 = new KeyGen256();
		keySet = keyGen256.generate(keySet);
		
		//////
		FileInputStream inputStream = null;
		try{
			inputStream = new FileInputStream(inputFile);
		}catch (FileNotFoundException exFileInput) {
			System.out.println("input file not found!!!");
			System.exit(1);
		}
		
		FileOutputStream outputStream = null;
		try{
			outputStream = new FileOutputStream(inputFile+".dec");
		}catch (FileNotFoundException exFileOutput) {
			System.out.println("dec output file tanked for some reason");
			System.exit(2);
		}
		
		byte[] byteblock = new byte[16];
		int[] intblock = new int[16];
		int[][] state = new int[4][4];
		SubByte subByte = new SubByte();
		MixColumn mixColumn = new MixColumn();
		
		long avai = 0;
		try{
			avai = inputStream.available();
		}catch (IOException inputstreamEX){
			System.out.println("inputStream not available ?");
			System.exit(3);
		}		
		//////
		for (long l=0; l<avai/16; l++) {
			zero(byteblock);
			int read = 0;
			try{
				read = inputStream.read(byteblock);
			} catch (IOException ioreadinputEX) {
				System.out.println("error read inputStream byteblock");
			}
			toIntArray(byteblock, intblock);
			
			for (int i=0; i<=15; i++){
				state[i/4][i%4] = intblock[i];
			}
			
//			printstate(state);
			//round 14
			state = addRoundKey(state, keySet, 14);
//			System.out.println("round 14 after addRoundKey");
//			printstate(state);
			state = shiftRowsDec(state);
//			System.out.println("round 14 after shiftRows");
//			printstate(state);
			state = subByte.backward(state);
//			System.out.println("round 14 after SubByte");
		
			for(int r=13; r>=1 ; r--) { //round 13-1
				state = addRoundKey(state, keySet, r);
//				System.out.println("round "+ r +" after addRoundKey");
//				printstate(state);
				state = mixColumn.mixDec(state);
//				System.out.println("round "+ r +" after mixColumns");
//				printstate(state);
				state = shiftRowsDec(state);
//				System.out.println("round "+ r +" after shiftRows");
//				printstate(state);
				state = subByte.backward(state);
//				System.out.println("round "+ r +" after SubByte");
//				printstate(state);
			}
			//round 0
			state = addRoundKey(state, keySet, 0);
//			System.out.println("round 0 after addRoundKey");
//			printstate(state);

			for (int i=0; i<=15; i++) {
				byteblock[i] = (byte) (state[i/4][i%4] & 0xFF); 
			}
			try{
				outputStream.write(byteblock,0,read);
			} catch (IOException outputStreamEX) {
				System.out.println("Enc Output Stream error");
			}
		}
	}
	
	
	public static void printstate(int[][] state){
		for (int i=0; i<16; i++){
			System.out.print(Integer.toHexString(state[i/4][i%4]).toUpperCase());
		}
		System.out.println();
	}
	
	public static int[][] shiftRowsEnc(int[][] state) {
		int t = state[0][1]; //row 1
		state[0][1]=state[1][1];
		state[1][1]=state[2][1];
		state[2][1]=state[3][1];
		state[3][1]=t;
		
		t= state[0][2]; //row 2
		state[0][2]= state[2][2];
		state[2][2]= t;
		t= state[1][2];
		state[1][2]= state[3][2];
		state[3][2]= t;
		
		t= state[3][3]; //row 3 . technically just do 1 right shift
		state[3][3]=state[2][3];
		state[2][3]=state[1][3];
		state[1][3]=state[0][3];
		state[0][3]=t;
		
		return state;
	}
	
	public static int[][] shiftRowsDec(int[][] state){
		int t = state[3][1]; //row 1
		state[3][1]=state[2][1];
		state[2][1]=state[1][1];
		state[1][1]=state[0][1];
		state[0][1]=t;
		
		t= state[0][2]; //row 2
		state[0][2]= state[2][2];
		state[2][2]= t;
		t= state[1][2];
		state[1][2]= state[3][2];
		state[3][2]= t;
		
		t= state[0][3]; //row 3 . technically just do 1 left shift
		state[0][3]=state[1][3];
		state[1][3]=state[2][3];
		state[2][3]=state[3][3];
		state[3][3]=t;
		
		return state;
	}
	
	public static int[][] addRoundKey(int[][] state, int[][] key, int round){
		for (int j= 4*round; j<= 4*round+3; j++) {
			state[j-(4*round)][0] = state[j-(4*round)][0]^key[j][0];
			state[j-(4*round)][1] = state[j-(4*round)][1]^key[j][1];
			state[j-(4*round)][2] = state[j-(4*round)][2]^key[j][2];
			state[j-(4*round)][3] = state[j-(4*round)][3]^key[j][3];				
		}
		return state;		
	}
	
	public static int[] toIntArray( byte[] b, int[] a ) {
		for(int i=0; i<b.length; i++)
			a[i] =(( b[i]  & 0xFF ));
		return a ;
	}
		
	public static byte[] zero( byte[] b ){
		for(int i=0; i<b.length; i++)
			b[i]=0;
		return b;
	}
}
